import json
import clipboard
import pyautogui
import time

# Load JSON data from a file
with open("tags.json", "r") as file:
    data = json.load(file)

# Wait for 10 seconds
time.sleep(10)

# Perform the pasting and typing process
for index, item in enumerate(data):
    # Create the clipboard content
    clipboard_text = f'.t add {item["name"]} {item["content"]}'
    
    # Copy the text to the clipboard
    clipboard.copy(clipboard_text)
    
    # Paste the text and press Enter
    pyautogui.hotkey('ctrl', 'v')
    pyautogui.press('enter')
    
    # Print the progress
    print(f"Total: {len(data)}, Done: {index + 1}")
    
    # Wait for a short duration between actions
    time.sleep(5)

# Prompt the user to press Enter to exit
input("Press Enter to exit...")

