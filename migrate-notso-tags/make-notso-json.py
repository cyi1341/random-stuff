import json

def read_json_file(filename):
    with open(filename, 'r') as file:
        json_data = file.read()
    return json.loads(json_data)

exported_data = read_json_file('exported.json')

filtered_data = [{"name": item["name"], "content": item["content"]} for item in exported_data]

with open('tags.json', 'w') as outfile:
    json.dump(filtered_data, outfile, indent=2)

