You need to first install https://git.sr.ht/~dax/xdcc, vim and Bun to be able to use it.

## Why

Torrents can die, scraping is inconsistent, so what's the solution?

Introducing `nibl-file-downloader`, where you use an ancient technology called XDCC (what even is that) and the biggest hottest JavaScr\*ipt runtime that is Bun, utilising the Bun Shell (nobody cares btw), along with an API from jikan.moe (that uses MAL), and most importantly the vim text editor (that is only used to edit the search term and folder name btw), to download files that are only made for kids (I think). 

No I'm not aware of what a streaming site is.

If you want this to be a "real" app, you can put it as an alias in something like a bashrc:

```bash
alias nibl-file-downloader='bun ~/random-stuff/nibl-file-downloader/script.js'
```

...or if you really want to, you can also build the file and make it a binary with `bun build script.js --compile`, it only takes up merely 90M of storage space on my Raspberry Pi, I'm sure that is fine for most people, right?
