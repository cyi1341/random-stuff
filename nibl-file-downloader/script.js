import { argv } from "process";
import { $ } from "bun";

function usage() {
  console.log("Usage: bun script.js -q <query>");
  process.exit(1);
}

// Parse command line options
const queryIndex = argv.indexOf("-q");
const query =
  queryIndex !== -1 && queryIndex + 1 < argv.length
    ? argv[queryIndex + 1]
    : null;

// Check if query is provided
if (!query) {
  usage();
}

async function getUserSelection(options, startIndex = 0) {
  const endIndex = Math.min(startIndex + 30, options.length);
  console.log("\nSelect a title or synonym:");
  options.slice(startIndex, endIndex).forEach((option, index) => {
    console.log(`${startIndex + index + 1}. ${option}`);
  });

  const answer = prompt(
    'Enter the number of your selection of "n" for next, "b" for back:',
  );

  if (answer === "n") {
    if (endIndex < options.length) {
      return await getUserSelection(options, startIndex + 30);
    } else {
      return null;
    }
  } else if (answer === "b") {
    if (startIndex > 0) {
      return await getUserSelection(options, startIndex - 30);
    } else {
      return null;
    }
  } else {
    const selection = parseInt(answer) - 1;
    if (selection >= startIndex && selection < endIndex) {
      return options[selection];
    } else {
      return null;
    }
  }
}

async function editWithVim(text) {
  const tempFile = "/tmp/edit_nibl.txt";
  await $`echo "${text}" > ${tempFile}`.quiet();
  await $`vim --clean --not-a-term ${tempFile}`;
  const editedContent = await $`cat ${tempFile}`.text();
  const firstLine = editedContent.split("\n")[0].trim();
  return firstLine;
}

function longestCommonPrefix(strs) {
  if (strs.length === 0) return "";
  let prefix = strs[0];
  for (let i = 1; i < strs.length; i++) {
    while (strs[i].indexOf(prefix) !== 0) {
      prefix = prefix.substring(0, prefix.length - 1);
      if (prefix === "") return "";
    }
  }
  // Remove any special character at the end of the string that isn't a normal English character or number
  while (prefix.length > 0 && !/[a-zA-Z0-9]$/.test(prefix)) {
    prefix = prefix.substring(0, prefix.length - 1);
  }
  return prefix;
}

async function searchNIBL(query, startIndex = 0) {
  const encodedQuery = encodeURIComponent(query);
  const url = `https://api.nibl.co.uk/nibl/search?query=${encodedQuery}`;

  try {
    const response = await fetch(url, {
      headers: { Accept: "application/json" },
    });
    const data = await response.json();

    if (data.content && data.content.length > 0) {
      const options = data.content.map((item) => item.name).reverse(); // Invert the list
      const botNames = await fetchBotNames(data.content);
      const packIds = await fetchPackIds(data.content);

      const endIndex = Math.min(startIndex + 30, options.length);

      const botNamesFinal = [];
      const packIdsFinal = [];
      console.log("\nSearch results:");
      options.slice(startIndex, endIndex).forEach((option, index) => {
        const botName =
          botNames[data.content.length - 1 - (startIndex + index)];
        const packId =
          packIds[data.content.length - 1 - (startIndex + index)];
        botNamesFinal[startIndex + index] = botName;
        packIdsFinal[startIndex + index] = packId;
        console.log(`${startIndex + index + 1}. ${botName}, ${option}`);
      });

      const answer = prompt(
        'Enter the number of your selection, "n" for next, "b" for back, or a range like "12-15,18":',
      );

      if (answer === "n") {
        if (endIndex < options.length) {
          await searchNIBL(query, startIndex + 30);
        } else {
          console.log("No more results.");
        }
      } else if (answer === "b") {
        if (startIndex > 0) {
          await searchNIBL(query, startIndex - 30);
        } else {
          console.log("No more results.");
        }
      } else {
        const ranges = answer
          .split(",")
          .map((range) => range.split("-").map(Number));
        const selectedIndices = ranges
          .flatMap((range) => {
            if (range.length === 1) {
              return range[0] - 1;
            } else {
              return Array.from(
                { length: range[1] - range[0] + 1 },
                (_, i) => range[0] + i - 1,
              );
            }
          })
          .filter((index) => index >= 0 && index < options.length);

        if (selectedIndices.length > 0) {
          const selectedItems = selectedIndices.map((index) => {
            return options[index];
          });
          console.log("Selected items:");
          selectedItems.forEach((item) => console.log(`- ${item}`));

          // Create a directory named after the longest common prefix among all selected filenames
          const longestCommonPrefixStr = longestCommonPrefix(selectedItems);

          const dirName = await editWithVim(longestCommonPrefixStr);
          await $`mkdir -p ${dirName}`;
          for (const index of selectedIndices) {
            const botName = botNamesFinal[index];
            const packId = packIdsFinal[index];
            let retries = 0;
            const maxRetries = 5;

            while (retries < maxRetries) {
              try {
                await $`xdcc -y -s irc.rizon.net -c nibl -b ${botName} ${packId}`.cwd(`./${dirName}`);
                break;
              } catch (error) {
                retries++;
                console.error(
                  `Error occurred, retrying... (attempt ${retries}/${maxRetries})`,
                );
              }
            }

            if (retries === maxRetries) {
              console.error(
                `Failed to execute command after ${maxRetries} retries`,
              );
            }
          }
        } else {
          console.log("No valid selection made.");
        }
        process.exit(0);
      }
    } else {
      console.log("No results found.");
    }
  } catch (error) {
    console.error("Error fetching NIBL search results:", error);
  }
}

async function fetchBotNames(content) {
  const botIds = content.map((item) => item.botId);
  const uniqueBotIds = [...new Set(botIds)];
  const botNames = {};

  for (const botId of uniqueBotIds) {
    const botUrl = `https://api.nibl.co.uk/nibl/bots/${botId}`;
    const response = await fetch(botUrl, {
      headers: { Accept: "application/json" },
    });
    const data = await response.json();
    botNames[botId] = data.content.name;
  }

  return content.map((item) => botNames[item.botId]);
}

async function fetchPackIds(content) {
  const packIds = content.map((item) => item.number);
  return packIds;
}

async function main() {
  try {
    const response = await fetch(
      `https://api.jikan.moe/v4/anime?q=${encodeURIComponent(query)}`,
    );
    const data = await response.json();

    const options = [];
    data.data.forEach((anime) => {
      const animeTitle = anime.title;
      const animeSynonyms = anime.titles;
      options.push(animeTitle);
      if (animeSynonyms) {
        animeSynonyms.forEach((synonym) => {
          if (synonym.type === "Synonym") {
            options.push(synonym.title);
          }
        });
      }
    });

    const selected = await getUserSelection(options);

    if (selected) {
      console.log(`Selected: ${selected}`);

      const editedQuery = await editWithVim(selected);
      console.log(`Edited query: ${editedQuery}`);

      await searchNIBL(editedQuery);
    } else {
      console.log("No valid selection made.");
    }
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
