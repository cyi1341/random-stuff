// ==UserScript==
// @name           Portals JSON Export
// @category       Info
// @author         cyi1341
// @version        1739934525
// @description    Exports a JSON file of all visible portals with full details about the team, resonators, links, etc.
// @id             portals-JSON
// @namespace      https://git.disroot.org/cyi1341
// @updateURL      https://git.disroot.org/cyi1341/random-stuff/raw/branch/main/portals-JSON-export/portals-JSON-export.meta.js
// @downloadURL    https://git.disroot.org/cyi1341/random-stuff/raw/branch/main/portals-JSON-export/portals-JSON-export.user.js
// @match          https://intel.ingress.com/*
// @match          https://intel-x.ingress.com/*
// @grant          none
// ==/UserScript==
