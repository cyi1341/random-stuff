// ==UserScript==
// @name           Portals JSON Export
// @category       Info
// @author         cyi1341
// @version        1739934525
// @description    Exports a JSON file of all visible portals with full details about the team, resonators, links, etc., with an option to include VPS PRODUCTION status for each portal.
// @id             portals-JSON
// @namespace      https://git.disroot.org/cyi1341
// @updateURL      https://git.disroot.org/cyi1341/random-stuff/raw/branch/main/portals-JSON-export/portals-JSON-export.meta.js
// @downloadURL    https://git.disroot.org/cyi1341/random-stuff/raw/branch/main/portals-JSON-export/portals-JSON-export.user.js
// @match          https://intel.ingress.com/*
// @match          https://intel-x.ingress.com/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
  if (typeof window.plugin !== 'function')
    window.plugin = function() {};

  // Use a unified plugin id
  plugin_info.pluginId = 'portalsJSON';
  window.plugin.portalsJSON = {};

  // Global caches, state, and helper variables:
  var portalsData = new Map();
  var linksCache = new Map();
  var fieldsCache = new Map();
  var startTime = null;
  var exportTime = null;
  var isExporting = false;
  var timerInterval = null;
  var portalDetailQueue = Promise.resolve();

  // Storage keys for each input/toggle
  const STORAGE_KEYS = {
    ONLINE_TOGGLE: 'portalsJSON_onlineToggle',
    VPS_TOGGLE: 'portalsJSON_vpsToggle',
    CORS_PROXY: 'portalsJSON_corsProxy',
    VPS_ENDPOINT: 'portalsJSON_vpsEndpoint',
    VPS_AUTH: 'portalsJSON_vpsAuth'
  };

  // Helper function to save value to localStorage
  function saveToStorage(key, value) {
    try {
      localStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      console.warn('Failed to save to localStorage:', e);
    }
  }

  // Helper function to load value from localStorage
  function loadFromStorage(key, defaultValue) {
    try {
      const stored = localStorage.getItem(key);
      return stored !== null ? JSON.parse(stored) : defaultValue;
    } catch (e) {
      console.warn('Failed to load from localStorage:', e);
      return defaultValue;
    }
  }

  /* ***************************************************************
     QUEUING & LOADING PORTAL DETAILS
  *************************************************************** */
  window.plugin.portalsJSON.queuePortalDetailRequest = function (guid) {
    return new Promise((resolve, reject) => {
      portalDetailQueue = portalDetailQueue.then(async () => {
        try {
          const details = await window.plugin.portalsJSON.loadSinglePortalDetails(guid);
          resolve(details);
        } catch (error) {
          reject(error);
        }
        await new Promise((r) => setTimeout(r, 100));
      });
    });
  };

  window.plugin.portalsJSON.loadSinglePortalDetails = function (guid) {
    return new Promise((resolve, reject) => {
      window.portalDetail.request(guid);
      const handler = function (data) {
        if (data.guid === guid) {
          window.removeHook('portalDetailLoaded', handler);
          if (data.details && Object.keys(data.details).length > 0) {
            resolve(data.details);
          } else {
            reject(new Error("No valid portal details received"));
          }
        }
      };
      window.addHook('portalDetailLoaded', handler);
    });
  };

  /* ***************************************************************
     EXPORT TIMER & REQUEST CHECKING
  *************************************************************** */
  window.plugin.portalsJSON.updateTimer = function () {
    var timerElement = document.querySelector("#exportTimer");
    if (!timerElement) return;

    if (startTime && !exportTime) {
      var elapsed = ((new Date()) - startTime) / 1000;
      timerElement.textContent = `Export Time: ${elapsed.toFixed(1)} seconds`;
    } else if (startTime && exportTime) {
      var total = ((exportTime - startTime) / 1000);
      timerElement.textContent = `Export Time: ${total.toFixed(3)} seconds`;
      clearInterval(timerInterval);
    }
  };

  window.plugin.portalsJSON.checkOngoingRequests = function () {
    return new Promise((resolve) => {
      function check() {
        if (
          window.mapDataRequest &&
          window.mapDataRequest.status &&
          ["loading", "refreshing", "paused", "startup"].includes(window.mapDataRequest.status.short)
        ) {
          setTimeout(check, 1000);
        } else {
          resolve();
        }
      }
      check();
    });
  };

  window.plugin.portalsJSON.startExport = async function () {
    if (isExporting) return;
    isExporting = true;
    startTime = new Date();
    exportTime = null;

    // Disable all export buttons
    document.querySelectorAll("#portalsJSON button").forEach((b) => (b.disabled = true));

    if (timerInterval) clearInterval(timerInterval);
    timerInterval = setInterval(window.plugin.portalsJSON.updateTimer, 10);

    // Wait until any ongoing map data requests are done.
    await window.plugin.portalsJSON.checkOngoingRequests();
  };

  window.plugin.portalsJSON.finishExport = function () {
    exportTime = new Date();
    isExporting = false;
    document.querySelectorAll("#portalsJSON button").forEach((b) => (b.disabled = false));
    window.plugin.portalsJSON.updateTimer();
  };

  /* ***************************************************************
     CACHE BUILDING (for live links and fields)
  *************************************************************** */
  function buildCaches() {
    linksCache.clear();
    fieldsCache.clear();

    Object.entries(window.links).forEach(([linkGuid, link]) => {
      const from = link.options.data.oGuid;
      const to = link.options.data.dGuid;
      if (!linksCache.has(from)) linksCache.set(from, { in: [], out: [] });
      linksCache.get(from).out.push(linkGuid);
      if (!linksCache.has(to)) linksCache.set(to, { in: [], out: [] });
      linksCache.get(to).in.push(linkGuid);
    });

    Object.entries(window.fields).forEach(([fieldGuid, field]) => {
      if (field.options.data && field.options.data.points && field.options.data.points.length === 3) {
        field.options.data.points.forEach((pt) => {
          const portalGuid = pt.guid;
          if (portalGuid) {
            if (!fieldsCache.has(portalGuid)) fieldsCache.set(portalGuid, []);
            fieldsCache.get(portalGuid).push(fieldGuid);
          }
        });
      }
    });
  }

  /* ***************************************************************
     FIELD DEFINITIONS & HELPER FUNCTIONS
  *************************************************************** */
  // Field definitions remain largely unchanged.
  window.plugin.portalsJSON.fields = [
    {
      title: "Portal Name",
      value: (portal) => portal.options.data.title,
    },
    {
      title: "Level",
      value: (portal) => portal.options.data.level,
    },
    {
      title: "Team",
      value: (portal) => {
        switch (portal.options.team) {
          case 0:
            return "Neutral";
          case 1:
            return "Resistance";
          case 2:
            return "Enlightened";
          case 3:
            return "__MACHINA__";
          default:
            return "Unknown";
        }
      },
    },
    {
      title: "Health",
      value: (portal) => portal.options.data.health,
    },
    {
      title: "Resonators",
      value: (portal) => portal.options.data.resCount,
    },
    {
      title: "AP",
      value: (portal) => {
        const links = portal.links;
        const fields = portal.fields;
        return portalApGainMaths(
          portal.options.data.resCount,
          links.in.length + links.out.length,
          fields.length
        );
      },
    },
    {
      title: "Latitude",
      value: (portal) => portal.getLatLng().lat,
    },
    {
      title: "Longitude",
      value: (portal) => portal.getLatLng().lng,
    },
    {
      title: "Image URL",
      value: (portal) => portal.options.data.image,
    },
    {
      title: "GUID",
      value: (portal) => portal.options.guid,
    },
    {
      title: "Incoming Links",
      value: (portal) =>
        (portal.links.in || []).map((linkGuid) => {
          const linkData = window.links[linkGuid].options.data;
          const targetGUID = linkData.oGuid;
          const lat = linkData.oLatE6 / 1e6;
          const lng = linkData.oLngE6 / 1e6;
          const data =
            (window.portals[targetGUID] && window.portals[targetGUID].options.data) ||
            window.portalDetail.get(targetGUID) ||
            {};
          return {
            GUID: targetGUID,
            Latitude: lat,
            Longitude: lng,
            "Portal Name": data.title,
          };
        }),
    },
    {
      title: "Outgoing Links",
      value: (portal) =>
        (portal.links.out || []).map((linkGuid) => {
          const linkData = window.links[linkGuid].options.data;
          const targetGUID = linkData.dGuid;
          const lat = linkData.dLatE6 / 1e6;
          const lng = linkData.dLngE6 / 1e6;
          const data =
            (window.portals[targetGUID] && window.portals[targetGUID].options.data) ||
            window.portalDetail.get(targetGUID) ||
            {};
          return {
            GUID: targetGUID,
            Latitude: lat,
            Longitude: lng,
            "Portal Name": data.title,
          };
        }),
    },
    {
      title: "Fields",
      value: (portal) => portal.fields,
    },
    {
      title: "Scanner Link",
      value: (portal) => {
        const ll = portal.getLatLng();
        return (
          "https://link.ingress.com/?link=https%3A%2F%2Fintel.ingress.com%2Fportal%2F" +
          portal.options.guid +
          "&apn=com.nianticproject.ingress&isi=576505181&ibi=com.google.ingress&ifl=https%3A%2F%2Fapps.apple.com%2Fapp%2Fingress%2Fid576505181&ofl=https%3A%2F%2Fintel.ingress.com%2Fintel%3Fpll%3D" +
          ll.lat +
          "%2C" +
          ll.lng
        );
      },
    },
  ];

  // For polygon exports we add two stub functions if not already defined.
  window.plugin.portalsJSON.circleToSearchCircle = function (drawnItem) {
    // Assuming drawnItem has getLatLng() and getRadius()
    return { type: "circle", center: drawnItem.getLatLng(), radius: drawnItem.getRadius() };
  };

  window.plugin.portalsJSON.pointIsInCircle = function (point, circle) {
    // Here we use a simple Euclidean distance approximation.
    const center = circle.center;
    const dx = point.lat - center.lat;
    const dy = point.lng - center.lng;
    const dist = Math.sqrt(dx * dx + dy * dy);
    return dist <= circle.radius;
  };

  /* ***************************************************************
     PROCESSING A SINGLE PORTAL
  *************************************************************** */
  window.plugin.portalsJSON.processPortal = function (portal) {
    return new Promise((resolve) => {
      // Early filtering by level/team
      const portalLevels = Array.from({ length: 8 }, (_, i) => i + 1);
      const teams = {
        "Unclaimed/Placeholder Portals": 0,
        Enlightened: 2,
        Resistance: 1,
        "__MACHINA__": 3,
      };

      // Filter out portals not meeting overlayStatus settings.
      for (let level of portalLevels) {
        if (!overlayStatus[`Level ${level} Portals`] && portal.options.data.level === level) {
          resolve();
          return;
        }
      }
      for (let [teamName, teamId] of Object.entries(teams)) {
        if (!overlayStatus[teamName] && portal.options.team === teamId) {
          resolve();
          return;
        }
      }
      if (!("title" in portal.options.data)) {
        resolve();
        return;
      }

      // Use cached links/fields if available.
      const guid = portal.options.guid;
      portal.links = linksCache.get(guid) || { in: [], out: [] };
      portal.fields = fieldsCache.get(guid) || [];

      let portalData = {};
      window.plugin.portalsJSON.fields.forEach((field) => {
        // For basic export, include all fields.
        const value = field.value(portal);
        portalData[field.title] = value;
      });
      portalsData.set(guid, portalData);
      resolve();
    });
  };

  window.plugin.portalsJSON.processPortalWithDetails = async function (portal, retries = 3) {
    const guid = portal.options.guid;
    let portalData = {};

    // Apply the same early filtering as in processPortal.
    const portalLevels = Array.from({ length: 8 }, (_, i) => i + 1);
    const teams = {
      "Unclaimed/Placeholder Portals": 0,
      Enlightened: 2,
      Resistance: 1,
      "__MACHINA__": 3,
    };

    for (let level of portalLevels) {
      if (!overlayStatus[`Level ${level} Portals`] && portal.options.data.level === level) return;
    }
    for (let [teamName, teamId] of Object.entries(teams)) {
      if (!overlayStatus[teamName] && portal.options.team === teamId) return;
    }
    if (!("title" in portal.options.data)) return;

    // Process the basic portal data (excluding AP, live links, and fields for now).
    window.plugin.portalsJSON.fields.forEach((field) => {
      // We handle AP, Incoming/Outgoing Links and Fields separately.
      if (["AP", "Incoming Links", "Outgoing Links", "Fields"].includes(field.title)) return;
      portalData[field.title] = field.value(portal);
    });

    // Use live caches.
    const liveLinks = window.plugin.portalsJSON.getLivePortalLinks(guid);
    portalData["Incoming Links"] = liveLinks.incoming;
    portalData["Outgoing Links"] = liveLinks.outgoing;
    portalData["Fields"] = window.plugin.portalsJSON.getLivePortalFields(guid);

    portalData["AP"] = portalApGainMaths(
      portal.options.data.resCount,
      liveLinks.incoming.length + liveLinks.outgoing.length,
      portalData["Fields"].length
    );

    // Attempt to get detailed data.
    let details = null;
    for (let i = 0; i < retries; i++) {
      try {
        details = await window.plugin.portalsJSON.loadPortalDetails(guid);
        break;
      } catch (err) {
        if (i === retries - 1) console.warn(`Failed to load details for portal ${guid}:`, err);
        await new Promise((res) => setTimeout(res, 1000));
      }
    }
    if (details) {
      const teamMapping = { N: "Neutral", R: "Resistance", E: "Enlightened", M: "__MACHINA__" };
      portalData["Team"] = teamMapping[details.team] || "Unknown";
      portalData["Mods"] = details.mods.map((mod) =>
        mod
          ? {
              owner: mod.owner,
              name: mod.name,
              rarity: mod.rarity,
              stats: mod.stats,
            }
          : null
      );
      portalData["Resonators"] = details.resonators.map((reson) =>
        reson
          ? {
              owner: reson.owner,
              level: reson.level,
              energy: reson.energy,
            }
          : null
      );
      portalData["Owner"] = details.owner;
    }
    portalsData.set(guid, portalData);
  };

  window.plugin.portalsJSON.loadPortalDetails = async function (guid, retries = 10) {
    let lastError;
    for (let i = 0; i < retries; i++) {
      try {
        const details = await window.plugin.portalsJSON.queuePortalDetailRequest(guid);
        return details;
      } catch (error) {
        lastError = error;
        if (i < retries - 1) await new Promise((r) => setTimeout(r, 1000));
      }
    }
    throw lastError || new Error("Failed to load portal details after " + retries + " attempts");
  };

  /* ***************************************************************
     HELPER: RUN EXPORT TASK
     This helper factors the common export steps (start, generate JSON,
     then upload/download, and finish)
  *************************************************************** */
  async function runExportTask(taskFn, filename) {
    try {
      await window.plugin.portalsJSON.startExport();
      const isOnline = document.getElementById("onlineToggle").checked;
      const corsProxyUrl = document.getElementById("corsProxyUrl").value;
      let jsonData = await taskFn();

      // NEW: Check if the user toggled export with VPS details.
      const vpsEnabled = document.getElementById("vpsToggle").checked;
      if (vpsEnabled) {
        // Parse the exported JSON into an array so we can add the extra field.
        const portalArray = JSON.parse(jsonData);
        await window.plugin.portalsJSON.addVpsDetails(portalArray);
        jsonData = JSON.stringify(portalArray, null, 2);
      }

      if (isOnline && jsonData.length <= 222 * 1024 * 1024) {
        await window.plugin.portalsJSON.uploadData(jsonData, filename, corsProxyUrl);
      } else {
        window.plugin.portalsJSON.downloadData(jsonData, filename);
      }
    } finally {
      window.plugin.portalsJSON.finishExport();
    }
  }

  /* ***************************************************************
     ADD VPS DETAILS FUNCTION
     This function takes the exported portal data (as an array) and
     batches portal GUIDs (up to 50 per request) to retrieve VPS details.
     For each portal, it adds a new field "Overclock" that is true if
     the portal's VPS detail shows vpsLocalizability as "PRODUCTION", false otherwise.
     If any batch request fails with a non-200 status or encounters a CORS error,
     it displays a message prompting the user to update the parameters. Then, when the user
     presses Enter in any of the text input fields, it retries the request.
  *************************************************************** */
  window.plugin.portalsJSON.addVpsDetails = async function (portalArray) {
    // Retrieve VPS API settings from the form.
    const baseUrl = "https://lightship.dev/v1/vps/getPoisByIds/";
    const endpointId = document.querySelector("#vpsApiEndpointId").value;
    const authToken = document.querySelector("#vpsAuthToken").value;
    const apiEndpoint = baseUrl + endpointId;
    // Get all GUIDs from the portal data.
    const portalGuids = portalArray.map((portal) => portal["GUID"]).filter(Boolean);

    // Helper function that waits for the user to press Enter in any of the text input boxes.
    async function waitForVpsParameterRetry() {
      return new Promise(resolve => {
        const messageId = 'vpsUpdateMessage';
        let messageDiv = document.getElementById(messageId);  //This will work for mobile and PC
        if (!messageDiv) {
          messageDiv = document.createElement('div');
          messageDiv.id = messageId;
          messageDiv.style.color = 'red';
          messageDiv.style.marginTop = '5px';
          messageDiv.textContent = 'Error fetching VPS details. Please update parameters (CORS Proxy, Endpoint ID, Auth Token) and press Enter in any input box to retry.';
            //Find container based on if it is mobile or PC
          let container;
          if (window.useAppPanes()){
              container = document.getElementById('portalsJSON');
          }
          else{
              container = document.getElementById('portal-JSON');
          }

          container.appendChild(messageDiv); //Correctly appends to dialog
        }

        function keyHandler(e) {
          if (e.key === 'Enter') {
            // Save the updated parameters to local storage (already handled by input event listeners)

            // Remove the message and event listeners
            if (messageDiv && messageDiv.parentNode) {
              messageDiv.parentNode.removeChild(messageDiv);
            }


              // Use querySelector on the container, NOT document
            const corsInput = container.querySelector("#corsProxyUrl");
            const endpointInput = container.querySelector("#vpsApiEndpointId");
            const authTokenInput = container.querySelector("#vpsAuthToken");

            corsInput.removeEventListener("keydown", keyHandler);
            endpointInput.removeEventListener("keydown", keyHandler);
            authTokenInput.removeEventListener("keydown", keyHandler);

            resolve();
          }
        }
          //Find container based on if it is mobile or PC
          let container;
          if (window.useAppPanes()){
              container = document.getElementById('portalsJSON');
          }
          else{
              container = document.getElementById('portal-JSON');
          }
        // Add event listeners - Add to each input to make sure any 'Enter' triggers retry
        container.querySelector("#corsProxyUrl").addEventListener("keydown", keyHandler);
        container.querySelector("#vpsApiEndpointId").addEventListener("keydown", keyHandler);
        container.querySelector("#vpsAuthToken").addEventListener("keydown", keyHandler);
      });
    }

    // Process in batches of 50.
    for (let i = 0; i < portalGuids.length; i += 50) {
      const batch = portalGuids.slice(i, i + 50);
      const payload = { poiIds: batch };

      let success = false;
      while (!success) {
        try {
          const corsProxyUrl = document.getElementById("corsProxyUrl").value;
          const currentEndpointId = document.getElementById("vpsApiEndpointId").value; // Get current values
          const currentAuthToken = document.getElementById("vpsAuthToken").value;
          const currentApiEndpoint = baseUrl + currentEndpointId;


          if (!corsProxyUrl) {
            alert("CORS Proxy URL is required for VPS functionality");
            return;
          }
          const response = await fetch(corsProxyUrl + encodeURIComponent(currentApiEndpoint), { // Use current values
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "Authorization": currentAuthToken, // Use current auth token
            },
            body: JSON.stringify(payload),
          });
          // If response was not successful (has a status code but not 200) then prompt user.
          if (!response.ok) {
            console.error(`VPS API request failed with status ${response.status}: ${response.statusText}`); // Log detailed error
            if (response.status === 401) {
              console.warn("Likely authentication issue (401 Unauthorized). Check your VPS Auth Token.");
            }
            await waitForVpsParameterRetry();
            continue;
          }
          const data = await response.json();
          if (data.pois && data.pois.length === batch.length) {
            for (let j = 0; j < batch.length; j++) {
              const vpsLocalizability = data.pois[j].vpsLocalizability;
              const overclock = (vpsLocalizability === "PRODUCTION");
              // Find and update the corresponding portal entry.
              let portal = portalArray.find(p => p["GUID"] === batch[j]);
              if (portal) {
                portal["Overclock"] = overclock;
              }
            }
            success = true;
          } else {
            console.warn("VPS API returned unexpected data format or length. Retrying parameter input.");
            await waitForVpsParameterRetry();
            continue;
          }
        } catch (err) {
          console.error("Error fetching VPS details:", err); // Log full error
          await waitForVpsParameterRetry();
          continue;
        }
      }
    }
  };

  /* ***************************************************************
     EXPORT TASKS (Portal data, Portal details, Polygons, etc.)
  *************************************************************** */
  window.plugin.portalsJSON.generateExportData = function () {
    portalsData.clear();
    startTime = new Date();
    buildCaches();
    const displayBounds = map.getBounds();
    const portalPromises = Object.values(window.portals)
      .filter((p) => displayBounds.contains(p.getLatLng()))
      .map((p) => window.plugin.portalsJSON.processPortal(p));
    return Promise.all(portalPromises).then(() =>
      JSON.stringify(Array.from(portalsData.values()), null, 2)
    );
  };

  window.plugin.portalsJSON.exportData = async function () {
    runExportTask(window.plugin.portalsJSON.generateExportData, "portals.json");
  };

  window.plugin.portalsJSON.exportDataWithDetails = async function () {
    // This task uses detailed data for portals within the current view.
    const taskFn = async () => {
      portalsData.clear();
      startTime = new Date();
      buildCaches();
      const displayBounds = map.getBounds();
      const promises = Object.values(window.portals)
        .filter((p) => displayBounds.contains(p.getLatLng()))
        .map((p) => window.plugin.portalsJSON.processPortalWithDetails(p));
      await Promise.all(promises);
      return JSON.stringify(Array.from(portalsData.values()), null, 2);
    };
    runExportTask(taskFn, "portals.json");
  };

  // Helper for polygon-related exports.
  function getPolygonPortalPromises(processFunc) {
    let searchItems = [];
    if (window.plugin.drawTools && window.plugin.drawTools.drawnItems) {
      window.plugin.drawTools.drawnItems.eachLayer(function (drawnItem) {
        if (drawnItem instanceof L.GeodesicCircle) {
          searchItems.push(window.plugin.portalsJSON.circleToSearchCircle(drawnItem));
        } else if (drawnItem instanceof L.GeodesicPolygon) {
          const polys = window.plugin.portalsJSON.multiPolygonToSearchPolygons(drawnItem);
          polys.forEach((pItem) => searchItems.push(pItem));
        }
      });
    }
    if (
      window.search.lastSearch &&
      window.search.lastSearch.selectedResult &&
      window.search.lastSearch.selectedResult.layer
    ) {
      window.search.lastSearch.selectedResult.layer.eachLayer(function (drawnItem) {
        if (drawnItem instanceof L.Polygon || (typeof L.MultiPolygon === "function" && drawnItem instanceof L.MultiPolygon)) {
          const polys = window.plugin.portalsJSON.multiPolygonToSearchPolygons(drawnItem);
          polys.forEach((pItem) => searchItems.push(pItem));
        }
      });
    }
    // Return array of promises based on whether a portal's point is in any search item.
    return Object.values(window.portals)
      .map(function (portal) {
        const point = portal.getLatLng();
        const found = searchItems.some((searchItem) => {
          switch (searchItem.type) {
            case "circle":
              return window.plugin.portalsJSON.pointIsInCircle(point, searchItem);
            case "polygon":
              return window.plugin.portalsJSON.pointIsInPolygon(point, searchItem);
            case "multipolygon":
              return window.plugin.portalsJSON.pointIsInMultiPolygon(point, searchItem.polygons);
            default:
              return false;
          }
        });
        if (found) return processFunc(portal);
      })
      .filter((p) => p);
  }

  // Standard polygon export.
  window.plugin.portalsJSON.exportPolygons = async function () {
    const taskFn = async () => {
      portalsData.clear();
      startTime = new Date();
      buildCaches();
      const promises = getPolygonPortalPromises(window.plugin.portalsJSON.processPortal);
      await Promise.all(promises);
      return JSON.stringify(Array.from(portalsData.values()), null, 2);
    };
    runExportTask(taskFn, "polygons.json");
  };

  // Polygons with detailed export
  window.plugin.portalsJSON.exportPolygonsWithDetails = async function () {
    const taskFn = async () => {
      portalsData.clear();
      startTime = new Date();
      buildCaches();
      const promises = getPolygonPortalPromises(window.plugin.portalsJSON.processPortalWithDetails);
      await Promise.all(promises);
      return JSON.stringify(Array.from(portalsData.values()), null, 2);
    };
    runExportTask(taskFn, "polygons.json");
  };

  // Inventory with details export:
  window.plugin.portalsJSON.loadInventory = function () {
    return new Promise((resolve, reject) => {
      window.postAjax(
        "getInventory",
        { lastQueryTimestamp: 0 },
        (data, textStatus, jqXHR) => resolve(data),
        (data, textStatus, jqXHR) => reject(data)
      );
    });
  };

  window.plugin.portalsJSON.processInventoryWithDetails = async function (guid) {
    try {
      const details = await window.plugin.portalsJSON.loadPortalDetails(guid);
      let portal = window.portals[guid];
      if (!portal) {
        console.error(`Portal with guid ${guid} not found`);
        return;
      }
      let portalData = {};
      window.plugin.portalsJSON.fields.forEach((field) => {
        portalData[field.title] = field.value(portal);
      });
      portalData["Mods"] = details.mods.map((mod) =>
        mod
          ? {
              owner: mod.owner,
              name: mod.name,
              rarity: mod.rarity,
              stats: mod.stats,
            }
          : null
      );
      portalData["Resonators"] = details.resonators.map((reson) =>
        reson
          ? {
              owner: reson.owner,
              level: reson.level,
              energy: reson.energy,
            }
          : null
      );
      portalData["Owner"] = details.owner;
      portalsData.set(guid, portalData);
    } catch (error) {
      console.error(`Failed to load details for portal ${guid}:`, error);
    }
  };

  window.plugin.portalsJSON.exportInventoryWithDetails = async function () {
    const taskFn = async () => {
      portalsData.clear();
      startTime = new Date();
      buildCaches();
      let tries = 0;
      async function tryLoadInventory() {
        try {
          return await window.plugin.portalsJSON.loadInventory();
        } catch (err) {
          if (++tries < 5) {
            await new Promise((res) => setTimeout(res, 3000));
            return tryLoadInventory();
          } else {
            throw new Error("Failed to load inventory after 5 tries");
          }
        }
      }
      let inventory = await tryLoadInventory();
      let portalGuids = new Set();
      inventory.result.forEach((item) => {
        if (item[2].resource && item[2].resource.resourceType === "PORTAL_LINK_KEY") {
          portalGuids.add(item[2].portalCoupler.portalGuid);
        } else {
          console.log(item[2].resource);
        }
      });
      const promises = Array.from(portalGuids).map((guid) => window.plugin.portalsJSON.processInventoryWithDetails(guid));
      await Promise.all(promises);
      return JSON.stringify(Array.from(portalsData.values()), null, 2);
    };
    runExportTask(taskFn, "inventory.json");
  };

  /* ***************************************************************
     LIVE DATA HELPERS (for details export)
  *************************************************************** */
  window.plugin.portalsJSON.getLivePortalLinks = function (guid) {
    const incoming = [];
    const outgoing = [];
    const cached = linksCache.get(guid) || { in: [], out: [] };
    cached.in.forEach((linkGuid) => {
      const link = window.links[linkGuid];
      if (link) {
        const d = link.options.data;
        incoming.push({
          GUID: d.oGuid,
          Latitude: d.oLatE6 / 1e6,
          Longitude: d.oLngE6 / 1e6,
          "Portal Name": window.portals[d.oGuid]?.options.data.title || "Unknown",
        });
      }
    });
    cached.out.forEach((linkGuid) => {
      const link = window.links[linkGuid];
      if (link) {
        const d = link.options.data;
        outgoing.push({
          GUID: d.dGuid,
          Latitude: d.dLatE6 / 1e6,
          Longitude: d.dLngE6 / 1e6,
          "Portal Name": window.portals[d.dGuid]?.options.data.title || "Unknown",
        });
      }
    });
    return { incoming, outgoing };
  };

  window.plugin.portalsJSON.getLivePortalFields = function (guid) {
    const guids = fieldsCache.get(guid) || [];
    return [...guids];
  };

  /* ***************************************************************
     UPLOAD & DOWNLOAD FUNCTIONS
  *************************************************************** */
  window.plugin.portalsJSON.uploadData = function (jsonData, filename, corsProxyUrl) {
    if (window.useAppPanes()) {
      $("#portalsJSON .export-result").remove();
    }
    const formData = new FormData();
    formData.append(
      "file",
      new Blob([jsonData], { type: "application/json;charset=utf-8" }),
      filename
    );
    formData.append("secret", "");
    fetch(corsProxyUrl + encodeURIComponent("https://envs.sh"), {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (!response.ok) throw new Error("Network response was not ok");
        return response.text();
      })
      .then((url) => {
        const cleanUrl = url.replace(/\n/g, "");
        const message =
          `File uploaded successfully.<br><br><a href="${cleanUrl}" target="_blank">Go to file</a><br><br>` +
          `Please consider donating to the host by visiting the front page: <a href="https://envs.sh" target="_blank">envs.sh</a>`;
        if (window.useAppPanes()) {
          $("#portalsJSON .export-result").remove();
          $("#portalsJSON").append('<div class="export-result">' + message + "</div>");
        } else {
          alert(message);
        }
        exportTime = new Date();
        window.plugin.portalsJSON.finishExport();
      })
      .catch((error) => {
        console.error("Error:", error);
        alert("Upload failed. Falling back to download.");
        window.plugin.portalsJSON.downloadData(jsonData, filename);
      });
  };

  window.plugin.portalsJSON.downloadData = function (jsonData, filename) {
    if (window.useAppPanes() && window.app && window.app.saveFile) {
      window.app.saveFile(filename, "application/json;charset=utf-8", jsonData);
    } else {
      const blob = new Blob([jsonData], { type: "application/json;charset=utf-8" });
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = filename;
      link.style.display = "none";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
    exportTime = new Date();
    window.plugin.portalsJSON.finishExport();
  };

  /* ***************************************************************
     MULTI-POLYGON HELPERS (point in polygon, etc.)
  *************************************************************** */
  window.plugin.portalsJSON.pnpoly = function (latlngs, point) {
    let inside = false;
    for (let i = 0, j = latlngs.length - 1; i < latlngs.length; j = i++) {
      if (
        ((latlngs[i].lat > point.lat) !== (latlngs[j].lat > point.lat)) &&
        point.lng <
          (latlngs[j].lng - latlngs[i].lng) * (point.lat - latlngs[i].lat) / (latlngs[j].lat - latlngs[i].lat) +
            latlngs[i].lng
      ) {
        inside = !inside;
      }
    }
    return inside;
  };

  window.plugin.portalsJSON.pointIsInPolygon = function (point, searchItem) {
    let inOuter = window.plugin.portalsJSON.pnpoly(searchItem.outerRing, point);
    if (!inOuter) return false;
    // Exclude the point if it's inside any hole.
    for (let i = 0; i < searchItem.holes.length; i++) {
      if (window.plugin.portalsJSON.pnpoly(searchItem.holes[i], point)) return false;
    }
    return true;
  };

  window.plugin.portalsJSON.pointIsInMultiPolygon = function (point, polygons) {
    return polygons.some((poly) => window.plugin.portalsJSON.pointIsInPolygon(point, poly));
  };

  window.plugin.portalsJSON.multiPolygonToSearchPolygons = function (drawnItem) {
    let result = [];
    let polygonArr = [];
    if (drawnItem instanceof L.GeodesicPolygon) {
      if (drawnItem._latlngs && drawnItem._latlngs.length > 0) {
        if (typeof drawnItem._latlngs[0].lng === "number") {
          polygonArr = [drawnItem._latlngs.map((pt) => [pt.lng, pt.lat])];
        } else if (drawnItem._latlngs[0] && drawnItem._latlngs[0][0] && typeof drawnItem._latlngs[0][0].lng === "number") {
          drawnItem._latlngs.forEach((latLngs) => {
            polygonArr.push(latLngs.map((pt) => [pt.lng, pt.lat]));
          });
        }
      }
    } else {
      polygonArr = drawnItem.toGeoJSON().geometry.coordinates;
      if (polygonArr[0].length === 2 && typeof polygonArr[0][0] === "number") {
        polygonArr = [polygonArr];
      }
    }
    polygonArr.forEach((polygonCoords) => {
      let searchPolygon = { type: "polygon", outerRing: [], holes: [] };
      if (polygonCoords[0].length === 2 && typeof polygonCoords[0][0] === "number") {
        polygonCoords = [polygonCoords];
      }
      polygonCoords.forEach((linearRing, index) => {
        let latLngArr = linearRing.map((coords) => ({ lng: coords[0], lat: coords[1] }));
        if (index === 0) searchPolygon.outerRing = latLngArr;
        else searchPolygon.holes.push(latLngArr);
      });
      result.push(searchPolygon);
    });
    return result;
  };

  /* ***************************************************************
     UI: DISPLAY EXPORT PANE
  *************************************************************** */
  window.plugin.portalsJSON.displayPL = function () {
    const toggleContainer = document.createElement("div");
    toggleContainer.style.marginBottom = "10px";

    const toggle = document.createElement("input");
    toggle.type = "checkbox";
    toggle.id = "onlineToggle";
    toggle.checked = loadFromStorage(STORAGE_KEYS.ONLINE_TOGGLE, false);
    toggle.addEventListener('change', () => {
      saveToStorage(STORAGE_KEYS.ONLINE_TOGGLE, toggle.checked);
    });

    const label = document.createElement("label");
    label.htmlFor = "onlineToggle";
    label.appendChild(document.createTextNode("Online Export"));

    toggleContainer.appendChild(toggle);
    toggleContainer.appendChild(label);

    // NEW: Toggle & form for VPS details export.
    const vpsContainer = document.createElement("div");
    vpsContainer.style.marginBottom = "10px";
    vpsContainer.style.border = "1px solid #ccc";
    vpsContainer.style.padding = "5px";

    const vpsToggle = document.createElement("input");
    vpsToggle.type = "checkbox";
    vpsToggle.id = "vpsToggle";
    vpsToggle.checked = loadFromStorage(STORAGE_KEYS.VPS_TOGGLE, false);
    vpsToggle.addEventListener('change', () => {
      saveToStorage(STORAGE_KEYS.VPS_TOGGLE, vpsToggle.checked);
    });

    const vpsLabel = document.createElement("label");
    vpsLabel.htmlFor = "vpsToggle";
    vpsLabel.appendChild(document.createTextNode("Export with VPS details"));

    vpsContainer.appendChild(vpsToggle);
    vpsContainer.appendChild(vpsLabel);

    const corsContainer = document.createElement("div");
    corsContainer.style.marginBottom = "10px";

    const corsLabel = document.createElement("label");
    corsLabel.htmlFor = "corsProxyUrl";
    corsLabel.appendChild(document.createTextNode("CORS Proxy URL:"));

    const corsInput = document.createElement("input");
    corsInput.type = "text";
    corsInput.id = "corsProxyUrl";
    corsInput.placeholder = "https://example-cors-proxy.com/?q=";
    corsInput.value = loadFromStorage(STORAGE_KEYS.CORS_PROXY, '');
    corsInput.addEventListener('input', () => {
      saveToStorage(STORAGE_KEYS.CORS_PROXY, corsInput.value);
    });

    corsContainer.appendChild(corsLabel);
    corsContainer.appendChild(corsInput);

    // VPS API Endpoint ID input.
    const vpsEndpointContainer = document.createElement("div");
    vpsEndpointContainer.style.marginBottom = "5px";
    const vpsEndpointLabel = document.createElement("label");
    vpsEndpointLabel.htmlFor = "vpsApiEndpointId";
    vpsEndpointLabel.appendChild(document.createTextNode("VPS API Endpoint ID:"));
    const vpsEndpointInput = document.createElement("input");
    vpsEndpointInput.type = "text";
    vpsEndpointInput.id = "vpsApiEndpointId";
    vpsEndpointInput.placeholder = "Input your endpoint";
    vpsEndpointInput.style.width = "100%";
    vpsEndpointInput.value = loadFromStorage(STORAGE_KEYS.VPS_ENDPOINT, '');
    vpsEndpointInput.addEventListener('input', () => {
      saveToStorage(STORAGE_KEYS.VPS_ENDPOINT, vpsEndpointInput.value);
    });
    vpsEndpointContainer.appendChild(vpsEndpointLabel);
    vpsEndpointContainer.appendChild(vpsEndpointInput);
    vpsContainer.appendChild(vpsEndpointContainer);

    // VPS Auth Token input.
    const vpsAuthContainer = document.createElement("div");
    vpsAuthContainer.style.marginBottom = "5px";
    const vpsAuthLabel = document.createElement("label");
    vpsAuthLabel.htmlFor = "vpsAuthToken";
    vpsAuthLabel.appendChild(document.createTextNode("VPS Auth Token:"));
    const vpsAuthInput = document.createElement("input");
    vpsAuthInput.type = "text";
    vpsAuthInput.id = "vpsAuthToken";
    vpsAuthInput.placeholder = "Enter your VPS auth token";
    vpsAuthInput.style.width = "100%";
    vpsAuthInput.value = loadFromStorage(STORAGE_KEYS.VPS_AUTH, '');
    vpsAuthInput.addEventListener('input', () => {
      saveToStorage(STORAGE_KEYS.VPS_AUTH, vpsAuthInput.value);
    });
    vpsAuthContainer.appendChild(vpsAuthLabel);
    vpsAuthContainer.appendChild(vpsAuthInput);
    vpsContainer.appendChild(vpsAuthContainer);

    // Create export buttons and wire them to our new export functions.
    const btnExportData = document.createElement("button");
    btnExportData.textContent = "Export Data to JSON";
    btnExportData.addEventListener("click", () => window.plugin.portalsJSON.exportData());
    btnExportData.style.display = "block";

    const btnExportPolygons = document.createElement("button");
    btnExportPolygons.textContent = "Export Polygons";
    btnExportPolygons.addEventListener("click", () => window.plugin.portalsJSON.exportPolygons());
    btnExportPolygons.style.display = "block";

    const btnExportDetails = document.createElement("button");
    btnExportDetails.textContent = "Export Data with Details";
    btnExportDetails.addEventListener("click", () => window.plugin.portalsJSON.exportDataWithDetails());
    btnExportDetails.style.display = "block";

    const btnExportPolyDetails = document.createElement("button");
    btnExportPolyDetails.textContent = "Export Polygons with Details";
    btnExportPolyDetails.addEventListener("click", () => window.plugin.portalsJSON.exportPolygonsWithDetails());
    btnExportPolyDetails.style.display = "block";

    const btnExportInventory = document.createElement("button");
    btnExportInventory.textContent = "Export Inventory Keys with Details";
    btnExportInventory.addEventListener("click", () => window.plugin.portalsJSON.exportInventoryWithDetails());

    // Check subscription to determine if the inventory button should be enabled.
    window.plugin.portalsJSON.checkSubscription((err, data) => {
      if (data && data.result === true) {
        btnExportInventory.style.display = "block";
        btnExportInventory.disabled = false;
      } else {
        btnExportInventory.style.display = "none";
      }
    });

    // Initially disable buttons, then enable when ready.
    [btnExportData, btnExportPolygons, btnExportDetails, btnExportPolyDetails, btnExportInventory].forEach(
      (btn) => (btn.disabled = true)
    );

    window.requestAnimationFrame(() => {
      [btnExportData, btnExportPolygons, btnExportDetails, btnExportPolyDetails].forEach(
        (btn) => (btn.disabled = false)
      );
    });

    const timerSpan = document.createElement("span");
    timerSpan.id = "exportTimer";

    // Assemble the container.
    const container = document.createElement("div");
    container.appendChild(toggleContainer);
    container.appendChild(vpsContainer);
    container.appendChild(corsContainer);
    container.appendChild(btnExportData);
    container.appendChild(btnExportPolygons);
    container.appendChild(btnExportDetails);
    container.appendChild(btnExportPolyDetails);
    container.appendChild(btnExportInventory);
    container.appendChild(timerSpan);

    if (window.useAppPanes()) {
      $("#portalsJSON").remove();
      $("<div id='portalsJSON' class='mobile'><div class='ui-dialog-titlebar'><span class='ui-dialog-title ui-dialog-title-active'>Portal JSON</span></div></div>").appendTo(document.body);
      $("#portalsJSON").append(container);
    } else {
      const dialogContainer = document.createElement("div");
      dialogContainer.appendChild(container);
      dialogContainer.setAttribute("class", "ui-dialog-portalsJSON");
      dialogContainer.title = "Portal JSON";
      dialogContainer.id = "portal-JSON";
      dialogContainer.style.width = "400px";
      dialog({ html: dialogContainer });
    }
    $("head").append(
      "<style>" +
        "#portalsJSON.mobile {background: transparent; border: 0 none !important; height: 100% !important; width: 100% !important; left: 0 !important; top: 0 !important; position: absolute; overflow: auto; z-index: 9000 !important;}" +
        "</style>"
    );
  };

  window.plugin.portalsJSON.checkSubscription = function (callback) {
    const versionStr = niantic_params.CURRENT_VERSION;
    const post_data = JSON.stringify({ v: versionStr });
    $.ajax({
      url: "/r/getHasActiveSubscription",
      type: "POST",
      data: post_data,
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      beforeSend: function (req) {
        req.setRequestHeader("accept", "*/*");
        req.setRequestHeader("X-CSRFToken", readCookie("csrftoken"));
      },
      success: (data) => callback(null, data),
      error: (data) => callback(data),
    });
  };

  window.plugin.portalsJSON.onPaneChanged = function (pane) {
    if (pane === "plugin-portalsJSON") window.plugin.portalsJSON.displayPL();
    else $("#portalsJSON").remove();
  };

  /* ***************************************************************
     SETUP
  *************************************************************** */
  var setup = function () {
    if (window.useAppPanes()) {
      app.addPane("plugin-portalsJSON", "Portals JSON", "ic_action_paste");
      addHook("paneChanged", window.plugin.portalsJSON.onPaneChanged);
    } else {
      const link = document.createElement("a");
      link.setAttribute("title", "Exports a JSON of portals in the current view [j]");
      link.setAttribute("accesskey", "j");
      link.textContent = "Portals JSON";
      link.addEventListener("click", function () {
        window.plugin.portalsJSON.displayPL();
      });
      document.querySelector("#toolbox").appendChild(link);
    }
  };
  setup.info = plugin_info;
  if (!window.bootPlugins) window.bootPlugins = [];
  window.bootPlugins.push(setup);
  if (window.iitcLoaded && typeof setup === "function") setup();
}

var script = document.createElement("script");
var info = {};
if (typeof GM_info !== "undefined" && GM_info && GM_info.script)
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description,
  };
script.appendChild(document.createTextNode("(" + wrapper + ")(" + JSON.stringify(info) + ")"));
(document.body || document.head || document.documentElement).appendChild(script);
