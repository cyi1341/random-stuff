# Portals-JSON-Export

[Download Here](https://git.disroot.org/cyi1341/random-stuff/raw/branch/main/portals-JSON-export/portals-JSON-export.user.js)

## Overview
This project is a modification of the portals-list from the IITC community edition. It aims to provide a more efficient and comprehensive data export solution for IITC users. Many exporters available online either do not provide sufficient information, are too restrictive, or both. The only viable alternative, portals-list (especially with the 3rd party addon), tends to be slow and resource-intensive with large datasets (10k+). Moreover, its export format isn't ideal for use with other programs or for data processing. This project aims to address these issues.

## Features
- Efficient data export
- Comprehensive information extraction
- Compatibility with other programs and data processing tools
- Export data with details including resonators, mods, and owner
- Export data within polygons drawn on the map
- JSON schema provided for both basic and detailed data

## Work in Progress
This project is still under development. Future updates will include features from other scripts to provide a more comprehensive solution. Here are some of the improvements to look forward to:

- A more detailed README, including credits
- Addition of a license

Please note that this list is not exhaustive and more features may be added based on user feedback and project requirements.
