import csv
from collections import defaultdict

# Read the CSV file
with open("spotify_playlist.csv", "r") as csvfile:
    reader = csv.DictReader(csvfile)
    data = [row for row in reader]

# Sort data by popularity
data.sort(key=lambda x: int(x['Popularity']), reverse=True)

# Top 15 most popular tracks
top_15_popular = data[:15]

# Top 15 least popular tracks
top_15_least_popular = data[-15:]

# Sort data by "Instrumental" value
data.sort(key=lambda x: float(x['Instrumental']), reverse=True)

# Top 15 most "Instrumental" tracks
top_15_instrumental = data[:15]

# Function to generate Markdown table
def generate_markdown_table(data, title):
    if not data:  # Check if data is empty
        return f"## {title}\n\nNo data available for this table.\n"

    headers = [field for field in data[0].keys() if field != "#"]
    
    # Move "Spotify Track Id" next to "Song"
    headers.remove("Spotify Track Id")
    song_index = headers.index("Song")
    headers.insert(song_index + 1, "Spotify Link")

    table = [headers]
    for row in data:
        new_row = []
        for key, value in row.items():
            if key == "#":
                continue
            if key == "Spotify Track Id":
                value = f"https://open.spotify.com/track/{value}"
                song_index = new_row.index(row["Song"])
                new_row.insert(song_index + 1, value)
                continue
            new_row.append(value)
        table.append(new_row)

    markdown_table = f"## {title}\n\n"
    markdown_table += "|".join(headers) + "\n"
    markdown_table += "|".join(["---"] * len(headers)) + "\n"
    for row in table[1:]:
        markdown_table += "|".join(row) + "\n"
    return markdown_table

# Generate Markdown tables
most_popular_table = generate_markdown_table(top_15_popular, "Top 15 Most Popular Tracks")
least_popular_table = generate_markdown_table(top_15_least_popular, "Top 15 Least Popular Tracks")
most_instrumental_table = generate_markdown_table(top_15_instrumental, "Top 15 Most Instrumental Tracks")

# Write to output file
with open("spotify_playlist.md", "w") as mdfile:
    mdfile.write(most_popular_table + "\n")
    mdfile.write(least_popular_table + "\n")
    mdfile.write(most_instrumental_table + "\n")

