# Auto Wayfarer (Gemini 1.5 Flash)

## Why
Educational purpose of course, you really think I made this to get the OPR onyx badge???

## Note
Google deprecated `gemini-pro-vision` and the model has now been updated to `gemini-1.5-flash`, and it now has a Request Per Day limit of 1500 which sucks, but hey the free tier stays free.

## How to use
If Google blocked your access of the Gemini 1.5 Flash API use a VPN first, then get the API key at Google AI (NOT Vertex AI), and replace your key on "MY_GEMINI_KEY".

You also need a Userscript manager like Tampermonkey, although pasting the code directly into the console may also work. The script is only tested on Firefox with Tampermonkey on Linux.

## What it doesn't do
I tried to prevent most errors from happening, but none of the functions regarding rejecting were tested throughly (only tested pieces of it in console), also Google would return an error if they think the image is bad, even if you have set every safety setting as "BLOCK_NONE".

Also it does not prevent reCaptchas from appearing, although I tried to make it so that it doesn't appear too frequently with the timeout values.

It also doesn't do anything with your Internet losing connection upon page refresh, as that's simply not what a Userscript is capable of doing.

## Why is it not doing anything/behaving weirdly?
Most likely your PC/Internet is slow and the default timeout isn't long enough to let it load everything, the script isn't made with auto detection in mind, so you'd need to set a higher timeout second for the values that causes fucnky stuff.

## Is it maintainable?
This code so far is my most easily readable one, but I still don't think it's maintainable.
