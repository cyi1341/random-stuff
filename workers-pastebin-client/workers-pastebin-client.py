import requests
import json
import argparse
from getpass import getpass

url = "https://your.worker.workers.dev/"

def login(username, password):
    data = {"username": username, "password": password}
    response = requests.post(f"{url}/login", json=data)
    return response.status_code, response.text

def search(key=None, value=None):
    data = {"key": key, "value": value}
    response = requests.post(f"{url}/search", json=data)
    return response.status_code, json.loads(response.text)

def delete(empty_field=False, individual=None, search=None, all_but=None, confirmation=None):
    data = {}
    if empty_field:
        data["emptyField"] = True
        if confirmation:
            data["confirmation"] = True
        else:
            return 400, "Confirmation required"
    elif individual:
        data["individual"] = individual
    elif search:
        data["search"] = search
    elif all_but:
        data["allBut"] = all_but
    else:
        return 400, "Invalid request"
    response = requests.post(f"{url}/delete", json=data)
    return response.status_code, response.text

def delete_all(confirmation=None):
    data = {"confirmation": confirmation}
    response = requests.post(f"{url}/delete-all", json=data)
    return response.status_code, response.text

def add_edit(key_value_pairs, replace_option=None, replace_keys=None):
    data = {"keyValuePairs": key_value_pairs}
    if replace_option:
        data["replaceOption"] = replace_option
        if replace_keys:
            data["replaceKeys"] = replace_keys
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(f"{url}/add-edit", headers=headers, json=data)
    return response.status_code, response.text

def main():
    parser = argparse.ArgumentParser(description="Interact with the pastebin API")
    parser.add_argument("action", choices=["login", "search", "delete", "delete-all", "add-edit"], help="The action to perform")
    parser.add_argument("--key", help="The key to search or delete")
    parser.add_argument("--value", help="The value to search")
    parser.add_argument("--empty-field", action="store_true", help="Delete all entries")
    parser.add_argument("--individual", help="Delete an individual entry")
    parser.add_argument("--search", help="Delete entries matching a key or value")
    parser.add_argument("--all-but", help="Delete all entries except those matching the key or value")
    parser.add_argument("--confirmation", action="store_true", help="Confirm deletion")
    parser.add_argument("--key-value-pairs", help="A JSON string of key-value pairs to add or edit")
    parser.add_argument("--replace-option", choices=["all", "choice"], help="The replace option to use")
    parser.add_argument("--replace-keys", help="A comma-separated list of keys to replace")
    args = parser.parse_args()

    if args.action == "login":
        username = input("Enter username: ")
        password = getpass("Enter password: ")
        status, result = login(username, password)
        print(f"Login status: {status}")
        print(f"Login result: {result}")
    elif args.action == "search":
        status, result = search(key=args.key, value=args.value)
        print(f"Search status: {status}")
        print(f"Search result: {result}")
    elif args.action == "delete":
        status, result = delete(empty_field=args.empty_field, individual=args.individual, search=args.search, all_but=args.all_but, confirmation=args.confirmation)
        print(f"Delete status: {status}")
        print(f"Delete result: {result}")
    elif args.action == "delete-all":
        status, result = delete_all(confirmation=args.confirmation)
        print(f"Delete all status: {status}")
        print(f"Delete all result: {result}")
    elif args.action == "add-edit":
        try:
            key_value_pairs = json.loads(args.key_value_pairs)
        except:
            print("Invalid key-value pairs")
            return
        status, result = add_edit(key_value_pairs, replace_option=args.replace_option, replace_keys=args.replace_keys.split(",") if args.replace_keys else None)
        print(f"Add/Edit status: {status}")
        print(f"Add/Edit result: {result}")
if __name__ == "__main__":
    main()
