# Directory Tree Generator

This Go program generates a directory tree and saves it into an XML file named `allFiles.txt`. It also includes the content of each file in the directory tree, each wrapped with its filename as an XML tag.

## How to Run

1. Ensure Go is installed on your machine. If not, you can download it from [here](https://golang.org/dl/).
2. Save the Go script into a file, for example `main.go`.
3. Open a terminal and navigate to the directory where you saved `main.go`.
4. Run the program by typing `go run main.go` and pressing `Enter`. Alternatively, you can also specify the directory path as an argument like so: `go run main.go /path/to/directory`.

## Sample Output

The output will be saved in a file named `allFiles.xml` in the same directory where you ran the script. The content of the file will look something like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<directoryTree>
  <node>
    <name>dir1</name>
    <isDir>true</isDir>
    <children>
      <node>
        <name>file1.txt</name>
        <isDir>false</isDir>
      </node>
      <node>
        <name>file2.txt</name>
        <isDir>false</isDir>
      </node>
    </children>
  </node>
  <node>
    <name>dir2</name>
    <isDir>true</isDir>
    <children>
      <node>
        <name>dir3</name>
        <isDir>true</isDir>
        <children>
          <node>
            <name>file3.txt</name>
            <isDir>false</isDir>
          </node>
        </children>
      </node>
      <node>
        <name>file4.txt</name>
        <isDir>false</isDir>
      </node>
    </children>
  </node>
  <file1.txt>
    <path>./dir1/file1.txt</path>
    <content>Hello, this is file1.</content>
  </file1.txt>
  <file2.txt>
    <path>./dir1/file2.txt</path>
    <content>This is another file in dir1.</content>
  </file2.txt>
  <file3.txt>
    <path>./dir2/dir3/file3.txt</path>
    <content>Nested file in dir3.</content>
  </file3.txt>
  <file4.txt>
    <path>./dir2/file4.txt</path>
    <content>File in dir2.</content>
  </file4.txt>
</directoryTree>
```

In this example, the directory structure is displayed first. Then, the contents of each file are shown, with the path to the file and its content wrapped in its filename tag.
