package main

import (
    "bufio"
    "encoding/xml"
    "fmt"
    "io/ioutil"
    "os"
    "os/user"
    "path/filepath"
    "sort"
    "strings"
)

type Node struct {
    XMLName  xml.Name `xml:"node"`
    Name     string   `xml:"name"`
    IsDir    bool     `xml:"isDir"`
    Children []*Node  `xml:"children"`
}

type FileContent struct {
    Path    string `xml:"path"`
    Content string `xml:",innerxml"`
}

func (n *Node) String(prefix string) string {
    var res strings.Builder
    res.WriteString(prefix)
    if !n.IsDir {
        res.WriteString(n.Name + "\n")
    } else {
        res.WriteString(n.Name + "\n")
        for i, child := range n.Children {
            if i == len(n.Children)-1 {
                res.WriteString(child.String(prefix + "└── "))
            } else {
                res.WriteString(child.String(prefix + "├── "))
            }
        }
    }
    return res.String()
}

func buildTree(root string) (*Node, error) {
    info, err := os.Stat(root)
    if err != nil {
        return nil, err
    }

    node := &Node{
        Name:  info.Name(),
        IsDir: info.IsDir(),
    }

    if node.IsDir {
        f, err := os.Open(root)
        if err != nil {
            return nil, err
        }
        defer f.Close()

        names, err := f.Readdirnames(-1)
        sort.Strings(names)
        if err != nil {
            return nil, err
        }

        node.Children = make([]*Node, len(names))
        for i, name := range names {
            node.Children[i], err = buildTree(filepath.Join(root, name))
            if err != nil {
                return nil, err
            }
        }
    }

    return node, nil
}

func expandPath(path string) (string, error) {
    if strings.HasPrefix(path, "~/") {
        usr, err := user.Current()
        if err != nil {
            return "", err
        }
        return filepath.Join(usr.HomeDir, path[2:]), nil
    }
    return filepath.Abs(path)
}

func main() {
    var dir string
    if len(os.Args) > 1 {
        dir = os.Args[1]
    } else {
        reader := bufio.NewReader(os.Stdin)
        fmt.Print("Enter directory: ")
        dir, _ = reader.ReadString('\n')
        dir = strings.TrimSuffix(dir, "\n")
    }

    dir, err := expandPath(dir)
    if err != nil {
        fmt.Println(err)
        return
    }

    tree, err := buildTree(dir)
    if err != nil {
        fmt.Println(err)
        return
    }

    f, err := os.Create("allFiles.txt")
    if err != nil {
        fmt.Println(err)
        f.Close()
        return
    }

    f.WriteString(xml.Header)
    f.WriteString("<directoryTree>\n")
    output, _ := xml.MarshalIndent(tree, "", "  ")
    f.WriteString(string(output) + "\n")

    filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
        if err != nil {
            return err
        }
        if !info.IsDir() {
            bytes, err := ioutil.ReadFile(path)
            if err != nil {
                fmt.Println(err)
            }
            fileContent := FileContent{
                Path:    path,
                Content: string(bytes),
            }
            f.WriteString("<" + info.Name() + ">\n")
            output, _ := xml.MarshalIndent(fileContent, "", "  ")
            f.WriteString(string(output) + "\n")
            f.WriteString("</" + info.Name() + ">\n")
        }
        return nil
    })

    f.WriteString("</directoryTree>\n")

    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return
    }

    fmt.Println("The directory tree has been saved in 'allFiles.txt'")
}
